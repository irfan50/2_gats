import * as React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/main.css'
import { StaticImage } from 'gatsby-plugin-image'
import { Link } from 'gatsby'

const RegCom = () => {
  return (
    <form className="form-signin text-center">
        <h1 class="h3 mb-3 font-weight-normal">Please sign up</h1>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="" />
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="" />
        <button className="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
        <Link to="/">Login</Link>
        <p className="mt-5 mb-3 text-muted">© 2017-2018</p>
    </form>
  )
}

export default {RegCom}