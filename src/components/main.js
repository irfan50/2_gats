import * as React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/main.css'
import { Link } from "gatsby";

const Navbar = () => {
   return (
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Company</h5>
      <Link class="btn btn-outline-primary order border-primary rounded-pill" to="/login">Login</Link>
    </div>
   )
}
const Home = () => {
  return (
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Coming Soon</h1>
    </div>
  )
}

export default {Navbar, Home}