import * as React from "react"
import Data from "../components/main"

const IndexPage = () => {
  return (
    <main>
      <Data.Navbar/>
      <Data.Home/>
    </main>
  )
}

export default IndexPage
