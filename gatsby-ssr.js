const React = require("react")
exports.onRenderBody = ({
  setHeadComponents,
}) => {
  setHeadComponents([
    <link
      key="1"
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
    />,
  ])
  setHeadComponents([
    <script
      key="2"
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    />,
  ])
}